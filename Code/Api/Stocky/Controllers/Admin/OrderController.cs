﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Stocky.Application.Service.Admin.Interface;
using Stocky.Model.Admin;
using Stocky.Model.Utility;
using Stocky.Utility;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Stocky.Controllers.Admin
{
    [Route(API_VERSION + "/order"), Authorize]
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost, ModelValidation]
        public async Task<IActionResult> Create([FromBody] OrderModel request)
        {
            try
            {
                return Ok(await _orderService.AddOrder(Request(request)));
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpGet("{id}"), ModelValidation]
        public async Task<IActionResult> Read(long id)
        {
            try
            {
                return Ok(await _orderService.GetSubOrderById(Request(id)));
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpGet, ModelValidation]
        public async Task<IActionResult> ReadAll(SearchRequest searchRequest)
        {
            try
            {
                return Ok(await _orderService.GetAllOrders(Request(searchRequest)));
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpPut("{orderId:long}/customerupdate"), ModelValidation]
        public async Task<IActionResult> UpdateCustomer([FromBody] OrderModel model)
        {
            try
            {
                await _orderService.UpdateCustomer(Request(model));
                return Ok();
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpPut("{orderId:long}/orderitem/{orderItemId:long}"), ModelValidation]
        public async Task<IActionResult> UpdateOrderItem([FromBody] OrderItemModel model)
        {
            try
            {
                await _orderService.UpdateOrderItem(Request(model));
                return Ok();
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

        [HttpDelete("{orderId:long}/orderitem/{orderItemId:long}"), ModelValidation]
        public async Task<IActionResult> DeleteOrderItem(long orderId, long orderItemId)
        {
            try
            {
                var item = new Dictionary<string, long>();
                item.Add("orderId", orderId);
                item.Add("orderItemId", orderItemId);
                await _orderService.DeleteOrderItem(Request(item));
                return Ok();
            }
            catch (Exception ex)
            {
                return await HandleException(ex);
            }
        }

    }
}
