﻿using System;
using System.ComponentModel.DataAnnotations;
using static Stocky.Common.Enums;

namespace Stocky.Model.Admin
{
    public class ChequePaymentModel
    {
        public virtual long Id { get; set; }
        public virtual long PartialPaymentId { get; set; }
        [Required]
        public virtual DateTime ChequeDate { get; set; }
        public virtual string Bank { get; set; }
        public virtual string Branch { get; set; }
        [Required]
        public virtual string ChequeNumber { get; set; }
        [Required]
        public virtual decimal Amount { get; set; }
        public virtual ChequeStatus ChequeStatus { get; set; }
    }
}
