﻿using System;
using static Stocky.Common.Enums;

namespace Stocky.Model.ViewResult
{
    public class ChequeListViewResult
    {
        public long OrderId { get; set; }
        public string CustomerName { get; set; }
        public virtual long Id { get; set; }
        public virtual long PartialPaymentId { get; set; }
        public virtual DateTime ChequeDate { get; set; }
        public virtual string Bank { get; set; }
        public virtual string Branch { get; set; }
        public virtual string ChequeNumber { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string ChequeStatus { get; set; }
    }
}
