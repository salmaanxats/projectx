﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stocky.Model.ViewResult
{
    public class ChequePaymentViewResult
    {
        public virtual long Id { get; set; }
        public virtual long PartialPaymentId { get; set; }
        public virtual DateTime ChequeDate { get; set; }
        public virtual string Bank { get; set; }
        public virtual string Branch { get; set; }
        public virtual string ChequeNumber { get; set; }
        public virtual decimal Amount { get; set; }
        public virtual string ChequeStatus { get; set; }
    }
}
