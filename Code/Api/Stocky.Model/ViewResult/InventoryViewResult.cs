﻿namespace Stocky.Model.ViewResult
{
    public class InventoryViewResult
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string SeperationFactor { get; set; }
        public string SeperationFactorValue { get; set; }
        public int AvailableQuantity { get; set; }
        public decimal InvoicedPrice { get; set; }
    }
}
