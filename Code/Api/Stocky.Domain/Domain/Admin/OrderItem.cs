﻿using Stocky.Domain.DomainModels.Admin;
using Stocky.Domain.DomainModels.Shared;
using Stocky.Model.Security;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using static Stocky.Common.Enums;

namespace Stocky.Core.Domain.Admin
{
    [Table("OrderItem", Schema = "Application")]
    public class OrderItem : BaseAuditable
    {
        [Required]
        public virtual long ProductId { get; protected set; }
        [Required]
        public virtual long ProductItemId { get; protected set; }
        [Required]
        public virtual decimal UnitPrice { get; protected set; }
        [Required]
        public virtual int Quantity { get; protected set; }
        [Required]
        public virtual decimal TotalAmount { get; protected set; }
        public virtual DiscountType DiscountType { get; protected set; }
        public virtual string DiscountTypeValue { get; protected set; }
        public virtual decimal Discount { get; protected set; }
        public virtual decimal TotalPayable { get; protected set; }
        public virtual long OrderId { get; set; }

        #region FK
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
        [ForeignKey("ProductItemId")]
        public virtual ProductList ProductList { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        #endregion

        public OrderItem()
        {

        }

        public OrderItem(UserModel user)
        {
            _user = user;
        }

        public OrderItem Create(long productId, long productItemId, decimal unitPrice, int qty, DiscountType discountType, 
            string discountTypeValue, decimal discount)
        {
            AuditableCreate();
            ProductId = productId;
            ProductItemId = productItemId;
            UnitPrice = unitPrice;
            Quantity = qty;
            DiscountType = discountType;
            Discount = discount;
            DiscountTypeValue = discountTypeValue;
            TotalAmount = UnitPrice * Quantity;
            TotalPayable = TotalAmount - Discount;
            return this;
        }

        public OrderItem Update(UserModel doingBy, long productItemId, int qty, DiscountType discountType, string discountTypeValue, decimal discount)
        {
            _user = doingBy;
            AuditableUpdate();
            Quantity = qty;
            DiscountType = discountType;
            Discount = discount;
            DiscountTypeValue = discountTypeValue;
            TotalAmount = UnitPrice * Quantity;
            TotalPayable = TotalAmount - Discount;
            Product.ProductList.Where(p => p.Id == productItemId).FirstOrDefault().UpdateAvailableQuantity(doingBy,qty);
            return this;
        }

        public OrderItem Delete(UserModel doingBy, long productItemId, int qty)
        {
            _user = doingBy;
            AuditableUpdate();
            Product.ProductList.Where(p => p.Id == productItemId).FirstOrDefault().UpdateAvailableQuantity(doingBy, qty);
            IsDeleted = true;
            return this;
        }
    }
}
