﻿using Stocky.Domain.DomainModels.Shared;
using Stocky.Model;
using Stocky.Model.Admin;
using Stocky.Model.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using static Stocky.Common.Enums;

namespace Stocky.Core.Domain.Admin
{
    [Table("Payment", Schema = "Application")]
    public class Payment : BaseAuditable
    {
        public virtual long OrderId { get; protected set; }
        [Required]
        public virtual decimal TotalAmount { get; protected set; }
        [Required]
        public virtual PaymentStatus PaymentStatus { get; protected set; }
        public virtual decimal Discount { get; protected set; }
        [Required]
        public virtual decimal TotalPayable { get; protected set; }
        public virtual long CustomerId { get; protected set; }
        public virtual List<PartialPayment> PartialPayment { get; set; } = new List<PartialPayment>();

        #region FK
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        #endregion

        public Payment()
        {

        }

        public Payment(UserModel user)
        {
            _user = user;
        }

        public Payment Create(long orderId, decimal totalAmount, decimal discount, long customerId)
        {
            AuditableCreate();
            CustomerId = customerId;
            PaymentStatus = PaymentStatus.Pending;
            OrderId = orderId;
            TotalAmount = totalAmount;
            Discount = discount;
            TotalPayable = TotalAmount - Discount;
            return this;
        }

        public Payment Update(UserModel doingby, decimal totalAmount, decimal discount, long customerId)
        {
            _user = doingby;
            AuditableUpdate();
            CustomerId = customerId;
            TotalAmount = totalAmount;
            Discount = discount;
            TotalPayable = TotalAmount - Discount;
            return this;
        }

        public Payment AddPartialPayment(UserModel doingBy, PartialPaymentModel item)
        {
            if (PartialPayment.Where(p=>p.PaymentStatus == PaymentStatus.Paid).Sum(p => p.PaidAmount) + item.PaidAmount > TotalPayable)
            {
                throw new Exception("Paid amount cannot exceed the total amount");
            }
            if (PartialPayment.Where(p => p.PaymentStatus == PaymentStatus.Paid).Sum(p => p.PaidAmount) + item.PaidAmount == TotalPayable)
            {
                PaymentStatus = PaymentStatus.Paid;
            }

            PartialPayment.Add(new PartialPayment(doingBy).Create(item.PaymentId, item.PaidAmount, item.PaymentMethod, PaymentStatus.Paid));
            return this;
        }

        public Payment AddChequePayment(UserModel doingBy, PartialChequePayment item)
        {
            PartialPayment.Add(new PartialPayment(doingBy).Create(item.PaymentId, item.PaidAmount, item.PaymentMethod, PaymentStatus.Pending)
                                                          .AddCheque(item.ChequePaymentModel.ChequeDate, item.ChequePaymentModel.Bank, 
                                                          item.ChequePaymentModel.Branch, item.ChequePaymentModel.ChequeNumber, item.ChequePaymentModel.Amount));
            return this;
        }

        public Payment UpdatePaymentStatus(ChequePaymentModel chequePaymentModel)
        {
            if (chequePaymentModel.ChequeStatus == ChequeStatus.Realized)
            {
                if (PartialPayment.Where(p => p.PaymentStatus == PaymentStatus.Paid).Sum(p => p.PaidAmount) + chequePaymentModel.Amount > TotalPayable)
                {
                    throw new Exception("Paid amount cannot exceed the total amount");
                }
                if (PartialPayment.Where(p => p.PaymentStatus == PaymentStatus.Paid).Sum(p => p.PaidAmount) + chequePaymentModel.Amount == TotalPayable)
                {
                    PaymentStatus = PaymentStatus.Paid;
                }
            }
            return this;
        }
    }
}
