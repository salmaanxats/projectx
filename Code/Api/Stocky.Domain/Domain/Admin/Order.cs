﻿using Stocky.Common;
using Stocky.Domain.DomainModels.Shared;
using Stocky.Model.Admin;
using Stocky.Model.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using static Stocky.Common.Enums;

namespace Stocky.Core.Domain.Admin
{
    [Table("Order", Schema = "Application")]
    public class Order : BaseAuditable
    {
        [Required]
        public virtual DateTime OrderDate { get; protected set; }
        [Required]
        public virtual decimal TotalAmount { get; protected set; }
        [Required]
        public virtual OrderStatus OrderStatus { get; protected set; }
        public virtual List<OrderItem> OrderItems { get; protected set; }
        public virtual Payment Payment { get; set; } = new Payment();
        public virtual long CustomerId { get; set; }

        #region FK
        [ForeignKey("CustomerId")]
        public virtual Customer Customer { get; set; }
        #endregion

        public Order()
        {

        }

        public Order(UserModel user)
        {
            _user = user;
        }

        public Order Create(long customerId)
        {
            AuditableCreate();
            OrderDate = DateTime.Now;
            OrderStatus = OrderStatus.Confirmed;
            CustomerId = customerId;
            return this;
        }

        public Order AddOrderItems(List<OrderItemModel> orderItems)
        {
            if (!orderItems.IsNullOrZero())
            {
                if (OrderItems.IsNullOrZero())
                {
                    OrderItems = new List<OrderItem>();
                }
                orderItems.ForEach(item =>
                {
                    OrderItems.Add(new OrderItem(_user).Create(item.ProductId, item.ProductItemId, item.UnitPrice, item.Quantity, item.DiscountType,
                        item.DiscountTypeValue, item.Discount));
                });
            }
            else
            {
                throw new RecordNotFoundException("Order items not found");
            }
            return this;
        }


        public Order AddToPayment()
        {
            var totalAmount = 0.0m;
            var totalDiscount = 0.0m;

            OrderItems.ForEach(item =>
            {
                totalAmount += item.UnitPrice * item.Quantity;
                totalDiscount += item.Discount;
            });
            TotalAmount = totalAmount - totalDiscount;
            Payment = new Payment(_user).Create(Id, totalAmount, totalDiscount, CustomerId);
            return this;
        }

        public Order UpdateCustomer(long customerId)
        {
            if (Payment.PaymentStatus != PaymentStatus.Pending)
                throw new InvalidOperationException("Cannot change values on Confirmed or cancelled orders");

            CustomerId = customerId;
            return this;
        }

        public Order UpdateOrderItem(UserModel doingBy, long productItemId, long orderItemId, int qty, DiscountType discountType, string discountTypeValue, decimal discount)
        {
            if (Payment.PaymentStatus != PaymentStatus.Pending)
                throw new InvalidOperationException("Cannot change values on Confirmed or cancelled orders");

            OrderItems.Where(p => p.Id == orderItemId).FirstOrDefault().Update(doingBy, productItemId, qty, discountType, discountTypeValue, discount);
            return this;
        }

        public Order DeleteOrderItem(UserModel doingBy, long orderItemId)
        {
            if (Payment.PaymentStatus != PaymentStatus.Pending)
                throw new InvalidOperationException("Cannot change values on Confirmed or cancelled orders");

            var item = OrderItems.Where(p => p.Id == orderItemId).FirstOrDefault();
            item.Delete(doingBy, item.ProductItemId, item.Quantity);
            OrderItems = OrderItems.Where(p => p.Id != orderItemId).ToList();
            return this;
        }

        public Order UpdatePayment(UserModel doingBy)
        {
            if (Payment.PaymentStatus != PaymentStatus.Pending)
                throw new InvalidOperationException("Cannot change values on Confirmed or cancelled orders");

            var totalAmount = 0.0m;
            var totalDiscount = 0.0m;

            OrderItems.ForEach(item =>
            {
                totalAmount += item.UnitPrice * item.Quantity;
                totalDiscount += item.Discount;
            });
            TotalAmount = totalAmount - totalDiscount;
            Payment.Update(doingBy, totalAmount, totalDiscount, CustomerId);
            return this;
        }
    }
}
