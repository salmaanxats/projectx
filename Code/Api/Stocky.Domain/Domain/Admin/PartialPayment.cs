﻿using Stocky.Domain.DomainModels.Shared;
using Stocky.Model.Admin;
using Stocky.Model.Security;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Stocky.Common.Enums;

namespace Stocky.Core.Domain.Admin
{
    [Table("PartialPayment", Schema = "Application")]
    public class PartialPayment : BaseAuditable
    {
        [Required]
        public virtual long PaymentId { get; protected set; }
        public virtual DateTime PaidDateTime { get; protected set; }
        [Required]
        public virtual decimal PaidAmount { get; protected set; }
        [Required]
        public virtual PaymentMethod PaymentMethod { get; protected set; }
        public virtual PaymentStatus PaymentStatus { get; protected set; }
        public virtual ChequePayment ChequePayment { get; set; }

        #region FK
        [ForeignKey("PaymentId")]
        public virtual Payment Payment { get; set; }
        #endregion

        public PartialPayment()
        {

        }

        public PartialPayment(UserModel user)
        {
            _user = user;
        }

        public PartialPayment Create(long paymentId, decimal paidAmount, PaymentMethod paymentMethod, PaymentStatus paymentStatus)
        {
            AuditableCreate();
            PaidDateTime = DateTime.Now;
            PaymentId = paymentId;
            PaidAmount = paidAmount;
            PaymentMethod = paymentMethod;
            PaymentStatus = paymentStatus;
            return this;
        }

        public PartialPayment SetPaymentStatus(ChequeStatus chequeStatus)
        {
            if (chequeStatus == ChequeStatus.Realized)
                PaymentStatus = PaymentStatus.Paid;
            return this;
        }

        public PartialPayment AddCheque(DateTime chequeDate, string bank, string branch, string chequeNumber,
                                decimal amount)
        {
            if (PaymentMethod != PaymentMethod.Cheque)
            {
                throw new InvalidOperationException("Payment method should be cheque to add cheques");
            }
            ChequePayment = new ChequePayment(_user).Create(Id, chequeDate, bank, branch, chequeNumber, amount, ChequeStatus.UnPresented);
            return this;
        }
    }
}
