﻿using Stocky.Domain.DomainModels.Shared;
using Stocky.Model.Security;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static Stocky.Common.Enums;

namespace Stocky.Core.Domain.Admin
{
    [Table("ChequePayment", Schema = "Application")]
    public class ChequePayment : BaseAuditable
    {
        public virtual long PartialPaymentId { get; set; }
        [Required]
        public virtual DateTime ChequeDate { get; set; }
        public virtual string Bank { get; set; }
        public virtual string Branch { get; set; }
        [Required]
        public virtual string ChequeNumber { get; set; }
        [Required]
        public virtual decimal Amount { get; set; }
        public virtual ChequeStatus ChequeStatus { get; set; }

        #region FK
        [ForeignKey("PartialPaymentId")]
        public virtual PartialPayment PartialPayment { get; set; }
        #endregion

        public ChequePayment()
        {

        }

        public ChequePayment(UserModel user)
        {
            _user = user;
        }

        public ChequePayment Create(long partialPaymentId, DateTime chequeDate, string bank, string branch, string chequeNumber,
                                decimal amount, ChequeStatus chequeStatus)
        {
            AuditableCreate();
            PartialPaymentId = partialPaymentId;
            ChequeDate = chequeDate;
            Bank = bank;
            Branch = branch;
            ChequeNumber = chequeNumber;
            Amount = amount;
            ChequeStatus = chequeStatus;
            return this;
        }

        public ChequePayment Delete(UserModel user)
        {
            _user = user;
            AuditableUpdate();
            IsDeleted = true;
            return this;
        }

        public ChequePayment Update(UserModel doingBy,DateTime chequeDate, string bank, string branch, string chequeNumber,
                               decimal amount, ChequeStatus chequeStatus)
        {
            _user = doingBy;
            this.AuditableUpdate();
            ChequeDate = chequeDate;
            Bank = bank;
            Branch = branch;
            ChequeNumber = chequeNumber;
            Amount = amount;
            ChequeStatus = chequeStatus;
            return this;
        }
    }
}
