﻿using Microsoft.EntityFrameworkCore;
using Stocky.Core.Domain.Admin;
using Stocky.Domain.DomainModels.Admin;
using Stocky.Domain.DomainModels.Shared.Security;

namespace Stocky.Core
{
    public class TwinlineEntityContext : DbContext
    {
        public TwinlineEntityContext(DbContextOptions<TwinlineEntityContext> options) : base(options)
        {
        }

        #region Security
        public DbSet<User> User { get; set; }
        #endregion

        #region Admin
        public DbSet<Category> Category { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductList> ProductList { get; set; }
        public DbSet<ProductImage> ProductImage { get; set; }
        public DbSet<Customer> Customer { get; set; }
        #endregion

        #region Order
        public DbSet<Order> Order { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<Payment> Payment { get; set; }
        public DbSet<PartialPayment> PartialPayment { get; set; }
        public DbSet<ChequePayment> ChequePayment { get; set; }
        #endregion

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //modelBuilder.Entity<Blog>().HasQueryFilter(b => EF.Property<string>(b, "_tenantId") == _tenantId);
            modelBuilder.Entity<Order>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<OrderItem>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Category>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<ChequePayment>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Customer>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Payment>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<PartialPayment>().HasQueryFilter(p => !p.IsDeleted);
            modelBuilder.Entity<Product>().HasQueryFilter(p => !p.IsDeleted);
        }
    }
}
