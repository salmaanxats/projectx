﻿using Microsoft.EntityFrameworkCore;
using Stocky.Application.Service.Admin.Interface;
using Stocky.Common;
using Stocky.Core.Domain.Admin;
using Stocky.Model;
using Stocky.Model.Admin;
using Stocky.Model.Utility;
using Stocky.Model.ViewResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stocky.Application.Service.Admin
{
    public class PaymentService : BaseService, IPaymentService
    {
        private readonly IChequeService _chequeService;

        public PaymentService(IChequeService chequeService, IBaseServiceInjector injector) : base(injector)
        {
            _chequeService = chequeService;
        }

        public async Task<PageList<PaymentViewResult>> GetAll(Request<SearchRequest> searchRequest)
        {
            var paymentListQuery = _injector._context.Payment
                                        .Include(p => p.Order)
                                        .Include(p => p.Customer)
                                        .Include(p => p.PartialPayment)
                                        .AsQueryable();
            if (!searchRequest.Item.SearchTerm.IsNullOrEmpty())
            {
                searchRequest.Item.SearchTerm = searchRequest.Item.SearchTerm.Trim();
                paymentListQuery = paymentListQuery.Where(p => EF.Functions.Like(p.Customer.Name, "%" + searchRequest.Item.SearchTerm + "%"));
            }
            var totalRecordCount = await paymentListQuery.CountAsync();
            var paymentList = await paymentListQuery.OrderByDescending(p => p.Id).Skip(searchRequest.Item.Skip).Take(searchRequest.Item.Take)
                                    .Select(p => new PaymentViewResult()
                                    {
                                        Id = p.Id,
                                        OrderId = p.OrderId,
                                        OrderDate = p.Order.OrderDate,
                                        CustomerName = p.Customer.Name,
                                        TotalAmount = p.TotalAmount,
                                        PaymentStatus = p.PaymentStatus.GetDescription(),
                                        Discount = p.Discount,
                                        TotalPayable = p.TotalPayable,
                                        RemainingAmount = p.PartialPayment.Count == 0 ? p.TotalPayable : p.TotalPayable - p.PartialPayment.Where(p => p.PaymentStatus == Enums.PaymentStatus.Paid).Sum(p => p.PaidAmount)
                                    }).ToListAsync();
            return new PageList<PaymentViewResult>(paymentList, searchRequest.Item.Skip, searchRequest.Item.Take, totalRecordCount);
        }

        public async Task<PaymentViewResult> GetPaymentById(Request<long> request)
        {
            var order = await _injector._context.Order
                                                .Include(p => p.Customer)
                                                .Include(p => p.Payment)
                                                .ThenInclude(p => p.PartialPayment)
                                                .ThenInclude(p => p.ChequePayment)
                                                .FirstOrDefaultAsync(p => p.Payment.Id == request.Item);

            var paymentViewResult = new PaymentViewResult();
            paymentViewResult.SetPayment(order.Payment.Id, order.Payment.OrderId, order.Payment.TotalAmount, order.Payment.PaymentStatus.GetDescription(),
                 order.Payment.Discount, order.Payment.TotalPayable, order.Payment.Customer.Name, order.OrderDate);
            paymentViewResult.PartialPayments = new List<PartialPaymentViewResult>();
            paymentViewResult.ChequePayments = new List<ChequePaymentViewResult>();

            order.Payment.PartialPayment.ForEach(item =>
            {
                if (item.PaymentStatus == Enums.PaymentStatus.Paid)
                {
                    paymentViewResult.PartialPayments.Add(new PartialPaymentViewResult()
                    {
                        Id = item.Id,
                        PaymentId = item.PaymentId,
                        PaidDateTime = item.PaidDateTime,
                        PaidAmount = item.PaidAmount,
                        PaymentMethod = item.PaymentMethod.GetDescription(),
                        PaymentStatus = item.PaymentStatus
                    });
                }
                if (!item.ChequePayment.IsNull())
                {
                    paymentViewResult.ChequePayments.Add(new ChequePaymentViewResult()
                    {
                        PartialPaymentId = item.Id,
                        Id = item.ChequePayment.Id,
                        ChequeDate = item.ChequePayment.ChequeDate,
                        Bank = item.ChequePayment.Bank,
                        Branch = item.ChequePayment.Branch,
                        ChequeNumber = item.ChequePayment.ChequeNumber,
                        Amount = item.ChequePayment.Amount,
                        ChequeStatus = item.ChequePayment.ChequeStatus.GetDescription()
                    });
                }
            });
            paymentViewResult.SetRemainingAmount(paymentViewResult.PartialPayments);
            return paymentViewResult;
        }

        public async Task AddPartialPayment(Request<PartialPaymentModel> request)
        {
            try
            {
                var payment = await _injector._context.Payment
                                        .Include(p => p.PartialPayment)
                                        .FirstOrDefaultAsync(p => p.Id == request.Item.PaymentId);

                payment.AddPartialPayment(request.User, request.Item);
                await _injector._context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task AddChequePayment(Request<PartialChequePayment> request)
        {
            try
            {
                var payment = await _injector._context.Payment
                                        .Include(p => p.PartialPayment)
                                        .FirstOrDefaultAsync(p => p.Id == request.Item.PaymentId);

                payment.AddChequePayment(request.User, request.Item);
                await _injector._context.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task UpdateChequePayment(Request<ChequePaymentModel> request)
        {
            using (var transaction = _injector._context.Database.BeginTransaction())
            {
                try
                {
                    var partialPayment = await _injector._context.PartialPayment.AsNoTracking().FirstOrDefaultAsync(p => p.Id == request.Item.PartialPaymentId);
                    var payment = await _injector._context.Payment
                                                .Include(p => p.PartialPayment)
                                                .FirstOrDefaultAsync(p => p.Id == partialPayment.PaymentId);
                    var selectedPartialPayment = payment.PartialPayment.FirstOrDefault(p => p.Id == request.Item.PartialPaymentId);
                    payment.UpdatePaymentStatus(request.Item);
                    selectedPartialPayment.SetPaymentStatus(request.Item.ChequeStatus);
                    await _chequeService.Update(request);
                    await _injector._context.SaveChangesAsync();
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }

            }
        }
    }
}
