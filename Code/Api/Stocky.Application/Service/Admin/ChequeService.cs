﻿using Microsoft.EntityFrameworkCore;
using Stocky.Application.Service.Admin.Interface;
using Stocky.Common;
using Stocky.Model.Admin;
using Stocky.Model.Utility;
using Stocky.Model.ViewResult;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stocky.Application.Service.Admin
{
    public class ChequeService : BaseService, IChequeService
    {
        public ChequeService(IBaseServiceInjector injector) : base(injector)
        {

        }


        public Task Add(Request<ChequePaymentModel> request)
        {
            throw new NotImplementedException();
        }

        public async Task Delete(Request<long> request)
        {
            var chequePayment = _injector._context.ChequePayment.FirstOrDefault(p => p.Id == request.Item);
            if (chequePayment.IsNull()) throw new Exception("Cheque not found");
            chequePayment.Delete(request.User);
            await _injector._context.SaveChangesAsync();
        }

        public async Task<ChequePaymentModel> Get(Request<long> request)
        {
            var chequePayment = await _injector._context.ChequePayment.FirstOrDefaultAsync(p => p.Id == request.Item);
            return _injector._mapper.Map<ChequePaymentModel>(chequePayment);
        }

        public async Task<PageList<ChequePaymentModel>> GetAll(Request<SearchRequest> searchRequest)
        {
            throw new NotImplementedException();
        }

        public async Task<PageList<ChequeListViewResult>> GetAllChequePayments(Request<SearchRequest> searchRequest)
        {
            var chequePaymentListQuery = _injector._context.ChequePayment
                                                           .Include(p => p.PartialPayment)
                                                           .ThenInclude(p => p.Payment)
                                                           .ThenInclude(p => p.Customer)
                                                           .AsQueryable();


            if (!searchRequest.Item.SearchTerm.IsNullOrEmpty())
            {
                searchRequest.Item.SearchTerm = searchRequest.Item.SearchTerm.Trim();
                chequePaymentListQuery = chequePaymentListQuery.Where(p => EF.Functions.Like(p.ChequeNumber, "%" + searchRequest.Item.SearchTerm + "%"));
            }
            var totalRecordCount = await chequePaymentListQuery.CountAsync();
            var chequePayment = await chequePaymentListQuery.OrderBy(p => p.ChequeDate).Skip(searchRequest.Item.Skip).Take(searchRequest.Item.Take)
                .Select(p => new ChequeListViewResult()
                {
                    Id = p.Id,
                    PartialPaymentId = p.PartialPaymentId,
                    OrderId = p.PartialPayment.Payment.OrderId,
                    CustomerName = p.PartialPayment.Payment.Customer.Company,
                    ChequeDate = p.ChequeDate,
                    ChequeNumber = p.ChequeNumber,
                    Bank = p.Bank,
                    Branch = p.Branch,
                    ChequeStatus = p.ChequeStatus.GetDescription(),
                    Amount = p.Amount
                }).ToListAsync();


            return new PageList<ChequeListViewResult>(chequePayment, searchRequest.Item.Skip, searchRequest.Item.Take, totalRecordCount);
        }

        public Task<List<KeyValuePair<long, string>>> KeyValue()
        {
            throw new NotImplementedException();
        }

        public async Task Update(Request<ChequePaymentModel> request)
        {
            var chequePayment = await _injector._context.ChequePayment.FirstOrDefaultAsync(p => p.Id == request.Item.Id);
            chequePayment.Update(request.User, request.Item.ChequeDate, request.Item.Bank, request.Item.Branch, request.Item.ChequeNumber, request.Item.Amount,
                request.Item.ChequeStatus);
            await _injector._context.SaveChangesAsync();
        }
    }
}
