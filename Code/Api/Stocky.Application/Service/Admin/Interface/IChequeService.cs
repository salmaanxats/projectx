﻿using Stocky.Application.Service.Shared;
using Stocky.Model.Admin;
using Stocky.Model.Utility;
using Stocky.Model.ViewResult;
using System.Threading.Tasks;

namespace Stocky.Application.Service.Admin.Interface
{
    public interface IChequeService : IBaseOperation<ChequePaymentModel>
    {
        Task<PageList<ChequeListViewResult>> GetAllChequePayments(Request<SearchRequest> searchRequest);
    }
}
