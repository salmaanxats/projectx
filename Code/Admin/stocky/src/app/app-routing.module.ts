import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout/layout/layout.component';
import { AuthGuardService } from './modules/auth/auth.guard.service';
import { LoginComponent } from './modules/auth/login/login.component';


const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: "full",
        canActivate: [AuthGuardService]
      },
      {
        path: 'dashboard',
        loadChildren: './modules/dashboard/dashboard.module#DashboardModule' ,
        canActivate: [AuthGuardService],
      },
      {
        path: 'category',
        loadChildren: './modules/category/category.module#CategoryModule',
        canActivate: [AuthGuardService],
      },
      {
        path: 'product',
        loadChildren: './modules/product/product.module#ProductModule',
        canActivate: [AuthGuardService],
      },
      {
        path: 'customer',
        loadChildren: './modules/customer/customer.module#CustomerModule',
        canActivate: [AuthGuardService],
      },
      {
        path: 'order',
        loadChildren: './modules/order/order.module#OrderModule',
        canActivate: [AuthGuardService],
      },
      {
        path: 'payment',
        loadChildren: './modules/payment/payment.module#PaymentModule',
        canActivate: [AuthGuardService],
      },
      {
        path: 'inventory',
        loadChildren: './modules/inventory/inventory.module#InventoryModule',
        canActivate: [AuthGuardService],
      },
    ]
  },
  {
    path: 'login',
    component: LoginComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
