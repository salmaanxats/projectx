import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderRoutingModule } from './order-routing.module';
import { OrderService } from './service/order.service';
import { OrderViewComponent } from './order-view/order-view.component';
import { OrderUpdateComponent } from './order-update/order-update.component';
import { ViewOrderReceiptComponent } from './view-order-receipt/view-order-receipt.component';
import { OrderInvoiceComponent } from './order-invoice/order-invoice.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [OrderViewComponent, OrderUpdateComponent, ViewOrderReceiptComponent, OrderInvoiceComponent],
  imports: [
    CommonModule,
    SharedModule,
    OrderRoutingModule
  ],
  providers: [OrderService]
})
export class OrderModule { }
