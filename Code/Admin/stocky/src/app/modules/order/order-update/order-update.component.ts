import { OrderService } from './../service/order.service';
import { OrderItemModel } from './../model/orderItemModel';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderModel } from '../model/orderModel';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { forkJoin } from 'rxjs';
import { ProductModel } from '../../product/model/productModel';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { KeyvalueService } from 'src/app/core/services/keyvalue.service';
import { DiscountType } from 'src/app/core/enums/discountType';
import { isUndefined, isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-order-update',
  templateUrl: './order-update.component.html',
  styleUrls: ['./order-update.component.scss']
})
export class OrderUpdateComponent implements OnInit {

  orderForm: FormGroup;
  product = new ProductModel();
  order = new OrderModel();
  orderItem = new OrderItemModel();
  orderItems = new Array<OrderItemModel>();

  lineItemIndex: number = 0;
  id: number;
  availableQuantity: number = 0;

  customers = [];
  products = [];
  productItems = [];

  isFormSubmitted = false;
  isOrderItemSubmitted = false;
  isEdit = false;
  isDiscountSelected: boolean = false;
  isBlocked: boolean = false;

  sectionName = "Order";

  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private orderService: OrderService, protected toasterService: ToasterService, private keyvalueService: KeyvalueService) {
  }

  ngOnInit() {
    this.createForm();
    this.initalize();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (!isZero(this.id) && !isUndefined(this.id)) {
        this.isEdit = true;
        this.getOrderById();
      }
    });
  }
  initalize() {
    this.isBlocked = true;
    forkJoin(
      this.keyvalueService.getCustomerKeyvalue(),
      this.keyvalueService.getProductKeyValue())
      .subscribe((res: any) => {
        this.customers = res[0];
        this.products = res[1];
        this.isBlocked = false;
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorLoading(error, "key values");
      });
  }

  onProductChange(event) {
    this.getProductItems(event.target.value);
  }

  getProductItems(productId) {
    this.isBlocked = true;
    this.orderService.getProductItems(productId)
      .subscribe((res: any) => {
        this.productItems = [];
        this.product = res;
        this.product.productList.forEach(element => {
          this.productItems.push({ key: element.id, value: element.seperationFactorValue });
        });
        this.isBlocked = false;
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorLoading(error, "Product items");
      });
  }

  onProductItemChange(event) {
    let selectedItemId = event.target.value;
    let selectedItem = this.product.productList.find(p => p.id == selectedItemId);
    this.orderItem.unitPrice = selectedItem.invoicedPrice;
    this.availableQuantity = selectedItem.availableQuantity;
  }

  createForm() {
    this.orderForm = this.fb.group({
      customerId: ['', [Validators.required]],
      orderItems: [[]]
    });
  }

  addOrderItem() {
    this.isFormSubmitted = true;
    if (this.orderForm.invalid) return;
    this.isOrderItemSubmitted = true;
    if (this.orderItem.quantity > this.availableQuantity || isUndefined(this.orderItem.quantity)) {
      this.toasterService.warning("Quantity should be less or equal to available quantity");
      return;
    }
    if (isZero(this.orderItem.quantity)) {
      this.toasterService.warning("Quantity should be greater than 0");
      return;
    }
    this.orderItem.setProductName(this.product);
    this.orderItem.setTotalAmount();
    this.orderItems.push(this.orderItem);
    this.orderItem = new OrderItemModel();
    this.availableQuantity = 0;
  }

  onDiscountTypeChange(event, item) {
    this.calculateDiscount(item);
    if (event.target.value != DiscountType.None) {
      this.isDiscountSelected = true;
    } else {
      this.isDiscountSelected = false;
    }
  }

  calculateDiscount(item: OrderItemModel) {
    item.calculateDiscount();
  }

  save() {
    if (this.orderItems.length == 0) return this.toasterService.warning("Cannot create an order without order items");
    this.isBlocked = true;
    this.order = Object.assign({}, this.order, this.orderForm.value);
    this.order.orderItems = this.orderItems;
    this.orderService.save(this.order)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.toasterService.successfullyCreated(this.sectionName);
        this.router.navigate(['order/invoice', res]);
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorSaving(error, this.sectionName);
      });
  }

  removeLineItem(index: number) {
    this.orderItems.splice(index, 1);
  }

  getOrderById() {
    this.isBlocked = true;
    this.orderService.getById(this.id)
      .subscribe((res: any) => {
        this.order = res;
        this.patchOrder(this.order);
        this.isBlocked = false;
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorLoading(error, this.sectionName);
      });
  }

  patchOrder(order: OrderModel) {
    this.orderForm.patchValue({
      customerId: order.customerId,
    });
    order.orderItems.forEach(ele => {
      var res = new OrderItemModel().create(ele);
      this.orderItems.push(res);
    });
    var item = this.orderItems.find(p => p.discountType != DiscountType.None);
    if (item != null)
      this.isDiscountSelected = true;
  }

  update() {
    this.isFormSubmitted = true;
    if (this.orderForm.invalid) return;
    if (this.orderItems.length == 0) return this.toasterService.warning("Can't create an order without order items");

    this.isBlocked = true;
    this.order = Object.assign({}, this.order, this.orderForm.value);
    this.order.orderItems = this.orderItems;
    this.orderService.updateCustomer(this.order)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.toasterService.successfullyUpdated(this.sectionName);
        this.router.navigate(['order']);
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorUpdating(error, this.sectionName);
      });
  }

  updateOrderItem(orderItemModel) {
    this.isBlocked = true;
    orderItemModel.orderId = this.id;
    this.orderService.updateOrderItem(this.id, orderItemModel)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.toasterService.successfullyUpdated("order item");
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorUpdating(error, this.sectionName);
      });
  }

  remove(index: number, orderItemModel) {
    this.isBlocked = true;
    orderItemModel.orderId = this.id;
    this.orderService.deleteOrderItem(this.id, orderItemModel.id)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.orderItems.splice(index, 1);
        this.toasterService.successfullyDeleted("order item");
      }, error => {
        this.isBlocked = false;
        this.toasterService.errorUpdating(error, this.sectionName);
      });
  }
}
