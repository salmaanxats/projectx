import { OrderService } from './../service/order.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-order-view',
  templateUrl: './order-view.component.html',
  styleUrls: ['./order-view.component.scss']
})
export class OrderViewComponent implements OnInit {

  orders = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);
  isBlocked: boolean = false;

  constructor(private fb: FormBuilder, private router: Router,
    private orderService: OrderService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (!isZero(data) && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getOrders();
    });
  }

  ngOnInit() {
    this.getOrders();
  }

  getOrders() {
    this.isBlocked = true;
    this.orderService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.orders = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "Order");
        });

  }

  editOrder(id: number) {
    this.router.navigate(['/order/update', id]);
  }

  viewInvoice(orderId: number) {
    this.router.navigate(['/order/invoice', orderId]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getOrders();
  }
}
