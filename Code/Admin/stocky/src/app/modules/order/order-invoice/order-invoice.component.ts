import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { OrderService } from '../service/order.service';
import { OrderModel } from '../model/orderModel';
import { CustomerModel } from '../../customer/model/customerModel';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { isUndefined, isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-order-invoice',
  templateUrl: './order-invoice.component.html',
  styleUrls: ['./order-invoice.component.scss']
})
export class OrderInvoiceComponent implements OnInit {

  today = new Date();
  id: number = 0;
  customer = new CustomerModel();
  invoice = new OrderModel();

  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private orderService: OrderService, protected toasterService: ToasterService) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (!isZero(this.id) && !isUndefined(this.id)) {
        this.getOrderById();
      }
    });
  }

  getOrderById() {
    this.orderService.getById(this.id)
      .subscribe((res: any) => {
        this.invoice = res;
        this.getCustomer();
      }, error => {
        this.toasterService.errorLoading(error, "Orders");
      });
  }

  getCustomer() {
    this.orderService.getOrderCustomer(this.invoice.customerId)
      .subscribe((res: any) => {
        this.customer = res;
      }, error => {
        this.toasterService.errorLoading(error, "Customer details");
      });
  }

  printInvoice() {
    window.print();
  }
}
