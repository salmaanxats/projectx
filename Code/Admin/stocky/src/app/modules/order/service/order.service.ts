import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';
import { BaseService } from 'src/app/core/services/base.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
import { OrderItemModel } from '../model/orderItemModel';
import { OrderModel } from '../model/orderModel';

@Injectable({
  providedIn: 'root'
})
export class OrderService extends BaseService implements IBaseImplementService<OrderModel>{

  baseLink: string;
  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "order";
  }
  update(body: OrderModel) {
    throw new Error('Method not implemented.');
  }
  get(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get);
  }
  getById(id: any) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Get)
  }
  save(body: OrderModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, body)
  }
  delete(id: any) {
    throw new Error('Method not implemented.');
  }
  updateCustomer(order) {
    return this.generateRequest(setup(`${this.baseLink}/${order.id}/customerupdate`), HttpOperation.Put, order)
  }
  getProductItems(productId) {
    return this.generateRequest(setup(`product/${productId}`), HttpOperation.Get)
  }
  getOrderCustomer(customerId) {
    return this.generateRequest(setup(`customer/${customerId}`), HttpOperation.Get)
  }
  updateOrderItem(orderId: number, orderItem: OrderItemModel) {
    return this.generateRequest(setup(`${this.baseLink}/${orderId}/orderitem/${orderItem.id}`), HttpOperation.Put, orderItem)
  }
  deleteOrderItem(orderId: number,orderItemId) {
    return this.generateRequest(setup(`${this.baseLink}/${orderId}/orderitem/${orderItemId}`), HttpOperation.Delete)
  }
}
