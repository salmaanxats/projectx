import { DiscountType } from 'src/app/core/enums/discountType';
import { ProductModel } from './../../product/model/productModel';

export class OrderItemModel {
  id: number;
  productName: string;
  productId: number;
  productItemId: number;
  unitPrice: number;
  quantity: number;
  totalAmount: number;
  discountType: DiscountType;
  discountTypeValue: number;
  discount: number;
  totalPayable: number;
  orderId: number;

  constructor() {
    this.discount = 0;
    this.discountType = DiscountType.None;
    this.totalAmount = 0;
    this.totalPayable = 0;
    this.productId = 0;
    this.quantity = 0;
    this.productItemId = 0;
    this.unitPrice = 0;
  }

  create(orderItemModel: this) {
    this.id = orderItemModel.id;
    this.productName = orderItemModel.productName;
    this.productId = orderItemModel.productId;
    this.productItemId = orderItemModel.productItemId;
    this.unitPrice = orderItemModel.unitPrice;
    this.quantity = orderItemModel.quantity;
    this.totalAmount = orderItemModel.totalAmount;
    this.discountType = orderItemModel.discountType;
    this.discountTypeValue = orderItemModel.discountTypeValue;
    this.discount = orderItemModel.discount;
    this.totalPayable = orderItemModel.totalPayable;
    this.orderId = orderItemModel.orderId;
    return this;
  }

  setProductName(product: ProductModel) {
    this.productName = product.name + " - " + product.productList.find(p => p.id == this.productItemId).seperationFactorValue;
  }

  setTotalAmount() {
    this.totalAmount = this.unitPrice * this.quantity;
    this.totalPayable = this.totalAmount;
  }

  calculateDiscount() {
    let type = Number(this.discountType);
    this.setTotalAmount();
    switch (type) {
      case DiscountType.None:
        {
          this.discountTypeValue = 0;
          this.discount = 0;
          break;
        }
      case DiscountType.Percentage:
        {
          this.discount = (this.totalAmount * this.discountTypeValue) / 100;
          this.totalPayable = this.totalAmount - this.discount
          break;
        }
      case DiscountType.Value:
        {
          this.discount = this.discountTypeValue;
          this.totalPayable = this.totalAmount - this.discount
          break;
        }
    }
  }
}
