import { CustomerModel } from './../model/customerModel';
import { CustomerService } from './../service/customer.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { isUndefined, isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-customer-update',
  templateUrl: './customer-update.component.html',
  styleUrls: ['./customer-update.component.scss']
})
export class CustomerUpdateComponent implements OnInit {

  customerForm: FormGroup;
  customer = new CustomerModel();

  id: number;
  isFormSubmitted = false;
  isEdit = false;
  isBlocked = false;
  sectionName = "Customer";

  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private customerService: CustomerService, protected toasterService: ToasterService) {
  }

  ngOnInit() {
    this.createForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (!isZero(this.id) && !isUndefined(this.id)) {
        this.isEdit = true;
        this.getCustomerById();
      }
    });
  }

  createForm() {
    this.customerForm = this.fb.group({
      name: ['', [Validators.required]],
      company: ['', [Validators.required]],
      contact: ['', [Validators.required]],
      address: ['', [Validators.required]],
      username: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  getCustomerById() {
    this.isBlocked = true;
    this.customerService.getById(this.id)
      .subscribe((res: any) => {
        this.customer = res;
        this.patchCustomer(this.customer);
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, this.sectionName);
        });
  }

  patchCustomer(customer: CustomerModel) {
    this.customerForm.patchValue({
      name: customer.name,
      company: customer.company,
      address: customer.address,
      contact: customer.contact
    });
  }

  save() {
    this.isFormSubmitted = true;
    if (this.customerForm.invalid) return;
    this.customer = Object.assign({}, this.customer, this.customerForm.value);
    this.isBlocked = true;
    this.customerService.save(this.customer)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.toasterService.successfullyCreated(this.sectionName);
        this.router.navigate(['customer/view']);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorSaving(error, this.sectionName);
        });
  }

  delete() {
    this.isBlocked = true;
    this.customerService.delete(this.id)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.router.navigate(['customer/view']);
        this.toasterService.successfullyDeleted(this.sectionName);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorDeleting(error, this.sectionName);
        });
  }

  update() {
    this.isFormSubmitted = true;
    if (this.customerForm.invalid) return;
    this.customer = Object.assign({}, this.customer, this.customerForm.value);
    this.isBlocked = true;
    this.customerService.update(this.customer)
      .subscribe((res: any) => {
        this.isBlocked = false;
        this.toasterService.successfullyCreated(this.sectionName);
        this.router.navigate(['customer/view']);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorUpdating(error, this.sectionName);
        });
  }
}
