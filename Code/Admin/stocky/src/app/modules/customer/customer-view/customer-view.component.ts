import { CustomerService } from './../service/customer.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';

@Component({
  selector: 'app-customer-view',
  templateUrl: './customer-view.component.html',
  styleUrls: ['./customer-view.component.scss']
})
export class CustomerViewComponent implements OnInit {

  customers = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);
  isBlocked = false;

  constructor(private fb: FormBuilder, private router: Router,
    private customerService: CustomerService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (data != "" && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getCustomers();
    });
  }

  ngOnInit() {
    this.getCustomers();
  }

  getCustomers() {
    this.isBlocked = true;
    this.customerService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.customers = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = true;
          this.toasterService.errorLoading(error, "Cusomers");
        });
  }

  editCustomer(id: number) {
    this.router.navigate(['/customer/update', id]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getCustomers();
  }
}
