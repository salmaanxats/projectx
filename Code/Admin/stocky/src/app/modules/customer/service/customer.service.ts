import { CustomerModel } from './../model/customerModel';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/services/base.service';
import { HttpClient } from '@angular/common/http';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService extends BaseService implements IBaseImplementService<CustomerModel>{
  baseLink: string;

  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "customer";
  }

  get(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get)
  }
  getById(id: any) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Get)
  }
  save(customerModel: CustomerModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, customerModel)
  }

  delete(id) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Delete)
  }

  update(customerModel: CustomerModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Put, customerModel)
  }
}
