import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerService } from './service/customer.service';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerUpdateComponent } from './customer-update/customer-update.component';
import { CustomerViewComponent } from './customer-view/customer-view.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
    declarations: [CustomerUpdateComponent, CustomerViewComponent],
    imports: [
        CommonModule,
        CustomerRoutingModule,
        SharedModule
    ],
    providers: [CustomerService]
})
export class CustomerModule { }
