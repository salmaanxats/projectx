import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { BaseService } from 'src/app/core/services/base.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { setupReadAll } from 'src/app/shared/utility/extension';

@Injectable({
  providedIn: 'root'
})
export class InventoryService extends BaseService {

  baseLink: string;

  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "inventory";
  }

  getInventory(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get);
  }
}
