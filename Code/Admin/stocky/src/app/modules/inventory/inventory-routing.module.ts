import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InventoryViewComponent } from './inventory-view/inventory-view.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full'
  },
  {
    path: 'view',
    component: InventoryViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
