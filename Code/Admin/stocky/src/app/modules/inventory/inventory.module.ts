import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InventoryViewComponent } from './inventory-view/inventory-view.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [InventoryViewComponent],
  imports: [
    CommonModule,
    SharedModule,
    InventoryRoutingModule
  ]
})
export class InventoryModule { }
