export class InventoryModel {
  id: number;
  name:string;
  category: string;
  seperationFactor :string;
  seperationFactorValue: string;
  availableQuantity: number;
  invoicedPrice: number;
}
