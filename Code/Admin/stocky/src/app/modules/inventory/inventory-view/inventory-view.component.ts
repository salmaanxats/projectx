import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { InventoryModel } from '../model/inventoryModel';
import { InventoryService } from '../service/inventory.service';

@Component({
  selector: 'app-inventory-view',
  templateUrl: './inventory-view.component.html',
  styleUrls: ['./inventory-view.component.scss']
})
export class InventoryViewComponent implements OnInit {

  isBlocked: boolean = false;
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);
  inventorList = new Array<InventoryModel>();


  constructor(private router: Router,
    private inventoryService: InventoryService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (data != "" && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getInventory();
    });
  }

  ngOnInit() {
    this.getInventory();
  }

  getInventory() {
    this.isBlocked = true;
    this.inventoryService.getInventory(this.searchRequestModel)
      .subscribe((res: any) => {
        this.inventorList = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "Order");
        });

  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getInventory();
  }
}
