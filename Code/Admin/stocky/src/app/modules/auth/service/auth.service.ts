import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { BaseService } from 'src/app/core/services/base.service';
import { setup } from 'src/app/shared/utility/extension';
import { LoginModel } from '../model/loginModel';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  constructor(protected http: HttpClient) {
    super(http);
  }

  authenticate(userLogin: LoginModel) {
    return this.generateRequest(setup("account/login"), HttpOperation.Post, userLogin);
  }
}
