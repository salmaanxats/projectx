import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginModel } from '../model/loginModel';
import { AuthService } from '../service/auth.service';
import { ToastrService } from 'ngx-toastr';
import { ToasterService } from 'src/app/core/services/toaster.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {

  userLoginForm: FormGroup;
  userLogin = new LoginModel();
  isRequested: boolean = false;

  constructor(protected http: HttpClient, private fb: FormBuilder, private router: Router,
    private authService: AuthService, protected toasterService: ToasterService) {
  }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.userLoginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  authenticate() {
    this.isRequested = true;
    this.userLogin = Object.assign({}, this.userLogin, this.userLoginForm.value);
    this.authService.authenticate(this.userLogin)
      .subscribe((res: any) => {
        this.isRequested = false;
        localStorage.setItem("Token", res.token);
        this.router.navigate(['dashboard']);
      },
        error => {
          this.isRequested = false;
          this.toasterService.warning(error,"Failed to login");
        });
  }
}
