import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ProductService } from '../service/product.service';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';

@Component({
  selector: 'app-product-view',
  templateUrl: './product-view.component.html',
  styleUrls: ['./product-view.component.scss']
})
export class ProductViewComponent implements OnInit {

  products = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);

  isBlocked = false;

  constructor(private fb: FormBuilder, private router: Router,
    private productService: ProductService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (data != "" && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getProducts();
    });
  }

  ngOnInit() {
    this.getProducts();
  }

  getProducts() {
    this.isBlocked = true;
    this.productService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.products = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "products");
        });
  }

  editProduct(id: number) {
    this.router.navigate(['/product/update', id]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getProducts();
  }

}
