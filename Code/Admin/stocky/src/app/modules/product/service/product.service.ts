import { ProductModel } from './../model/productModel';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/services/base.service';
import { HttpClient } from '@angular/common/http';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService extends BaseService implements IBaseImplementService<ProductModel>{
  baseLink: string;

  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "product";
  }
  get(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get)
  }
  getById(id: any) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Get)
  }
  getCategoryKeyValue() {
    return this.generateRequest(setup("category/keyvalue"), HttpOperation.Get)
  }
  saveWithImage(productModel: ProductModel, files: FileList) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.FilesPost, productModel, files)
  }

  delete(id) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Delete)
  }
  updateWithImage(productModel: ProductModel, files: FileList) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.FilesPut, productModel, files)
  }

  //#region not implmented

  save(body: ProductModel) {
    throw new Error('Method not implemented.');
  }
  update(body: ProductModel) {
    throw new Error('Method not implemented.');
  }
}
