import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentRoutingModule } from './payment-routing.module';
import { PaymentViewComponent } from './payment-view/payment-view.component';
import { PaymentUpdateComponent } from './payment-update/payment-update.component';
import { PaymentService } from './service/payment.service';
import { ChequePaymentService } from './service/chequePayment.service';
import { ChequePaymentViewComponent } from './cheque-payment-view/cheque-payment-view.component';
import { SharedModule } from 'src/app/shared/shared.module';

@NgModule({
  declarations: [PaymentViewComponent, PaymentUpdateComponent, ChequePaymentViewComponent],
  imports: [
    CommonModule,
    PaymentRoutingModule,
    SharedModule
  ],
  providers: [PaymentService, ChequePaymentService]
})
export class PaymentModule { }
