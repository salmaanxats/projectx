import { ChequeStatus } from 'src/app/core/enums/chequeStatus';
import { PartialPaymentModel } from './partialPaymentModel';

export class ChequePaymentModel {
  id: number;
  paymentId: number;
  paidDateTime: Date;
  paidAmount: number;
  paymentMethod: number;
  chequePaymentModel: ChequeModel;
}

export class ChequeModel {
  id: number;
  partialPaymentId: number;
  chequeDate: Date;
  bank: string;
  branch: string;
  chequeNumber: string;
  amount: number;
  chequeStatus: ChequeStatus;
}
