export class PartialPaymentModel {
  id: number;
  paymentId: number;
  paidDateTime: Date;
  paidAmount: number;
  paymentMethod: string;
}
