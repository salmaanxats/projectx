import { PartialPaymentModel } from './partialPaymentModel';
import { ChequeModel } from './checkPaymentModel';

export class PaymentModel {
  id: number;
  orderId: number;
  orderDate: Date;
  totalAmount: number;
  paymentStatus: string;
  discount: number;
  totalPayable: number;
  remainingAmount: number;
  customerName: string;
  partialPayments = new Array<PartialPaymentModel>();
  chequePayments = new Array<ChequeModel>();
}
