import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PartialPaymentModel } from '../model/partialPaymentModel';
import { BaseService } from 'src/app/core/services/base.service';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';

@Injectable({
  providedIn: 'root'
})
export class PaymentService extends BaseService implements IBaseImplementService<PartialPaymentModel>{

  baseLink: string;
  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "payment";
  }
  get(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get)
  }
  getById(id: any) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Get)
  }
  save(body: PartialPaymentModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, body)
  }
  delete(id: any) {
    throw new Error('Method not implemented.');
  }
  update(body: PartialPaymentModel) {
    throw new Error('Method not implemented.');
  }
}
