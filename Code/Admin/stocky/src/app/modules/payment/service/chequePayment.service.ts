import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';
import { BaseService } from 'src/app/core/services/base.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
;
import { ChequeModel, ChequePaymentModel } from '../model/checkPaymentModel';

@Injectable({
  providedIn: 'root'
})
export class ChequePaymentService extends BaseService implements IBaseImplementService<ChequeModel> {
  baseLink: string;

  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "payment/cheque";
  }
  get(searchRequestModel: SearchRequestModel) {
    return this.generateRequest(setupReadAll("chequepayment", searchRequestModel), HttpOperation.Get)
  }
  getById(id: any) {
    throw new Error('Method not implemented.');
  }
  save(body: ChequeModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, body)
  }
  delete(id: any) {
    throw new Error('Method not implemented.');
  }
  update(body: ChequeModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Put, body)
  }
  addCheque(chequePaymentModel: ChequePaymentModel) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, chequePaymentModel)
  }

}
