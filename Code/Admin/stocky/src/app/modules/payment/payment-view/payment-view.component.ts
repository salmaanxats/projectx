import { PaymentService } from './../service/payment.service';
import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-payment-view',
  templateUrl: './payment-view.component.html',
  styleUrls: ['./payment-view.component.scss']
})
export class PaymentViewComponent implements OnInit {

  payments = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);
  isBlocked: boolean = false;

  constructor(private router: Router,
    private paymentService: PaymentService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (!isZero(data) && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getPayments();
    });
  }

  ngOnInit() {
    this.getPayments();
  }

  getPayments() {
    this.isBlocked = true;
    this.paymentService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.payments = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "Payments");
        });
  }

  editPayment(id: number) {
    this.router.navigate(['/payment/update', id]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getPayments();
  }
}
