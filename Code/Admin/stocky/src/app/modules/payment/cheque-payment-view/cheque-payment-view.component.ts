import { ChequePaymentService } from './../service/chequePayment.service';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { Subject } from 'rxjs';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ChequeModel } from '../model/checkPaymentModel';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-cheque-payment-view',
  templateUrl: './cheque-payment-view.component.html',
  styleUrls: ['./cheque-payment-view.component.scss']
})
export class ChequePaymentViewComponent implements OnInit {

  chequePayments = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);
  isBlocked: boolean = false;
  isChequePaymentFormSubmitted: boolean = false;
  chequeModel = new ChequeModel();
  selectedChequeModel = new ChequeModel();

  chequeForm: FormGroup;
  modalRef: BsModalRef;

  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(private fb: FormBuilder, private router: Router,
    private chequePaymentService: ChequePaymentService, protected toasterService: ToasterService,
    private modalService: BsModalService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (!isZero(data) && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getChequePayments();
    });
  }

  ngOnInit() {
    this.getChequePayments();
    this.createChequeForm();
  }

  createChequeForm() {
    this.chequeForm = this.fb.group({
      chequeDate: [new Date, [Validators.required]],
      bank: [''],
      branch: [''],
      chequeNumber: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      chequeStatus: ['']
    });
  }

  openModal(template: TemplateRef<any>, isUpdate: boolean, chequeModel: ChequeModel) {
    if (isUpdate && chequeModel.chequeStatus.toString() != 'Realized') {
      this.modalRef = this.modalService.show(template, this.config);
    } else {
      this.toasterService.warning("Cannot edit realized cheques");
      return;
    }

    this.selectedChequeModel = chequeModel;
    this.patchChequePayment(chequeModel);
  }

  patchChequePayment(chequeModel: ChequeModel) {
    this.chequeForm.patchValue({
      chequeDate: new Date(chequeModel.chequeDate),
      bank: chequeModel.bank,
      branch: chequeModel.branch,
      chequeNumber: chequeModel.chequeNumber,
      amount: chequeModel.amount,
      chequeStatus: this.getChequeStatus(chequeModel.chequeStatus)
    });
  }


  getChequeStatus(chequeStatus) {
    if (chequeStatus == "Unrealized")
      return 1;
    if (chequeStatus == "Realized")
      return 2;
    if (chequeStatus == "Bounced")
      return 3;
    if (chequeStatus == "UnPresented")
      return 4;
  }

  getChequePayments() {
    this.isBlocked = true;
    this.chequePaymentService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.chequePayments = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "Cheque Paymentss");
        });
  }

  editCheque(id: number) {
    this.router.navigate(['/payment/update', id]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getChequePayments();
  }

  updateCheque() {
    this.isChequePaymentFormSubmitted = true;
    if (this.chequeForm.invalid) return;
    this.chequeModel = Object.assign({}, this.chequeModel, this.chequeForm.value);
    this.chequeModel.id = this.selectedChequeModel.id;
    this.chequeModel.partialPaymentId = this.selectedChequeModel.partialPaymentId;
    this.isBlocked = true;
    this.chequePaymentService.update(this.chequeModel)
      .subscribe((res: any) => {
        this.toasterService.successfullyCreated("Cheque");
        this.modalRef.hide();
        this.getChequePayments();
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorUpdating(error, "Cheque");
        });
  }
}
