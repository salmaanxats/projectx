import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PaymentViewComponent } from './payment-view/payment-view.component';
import { PaymentUpdateComponent } from './payment-update/payment-update.component';
import { ChequePaymentViewComponent } from './cheque-payment-view/cheque-payment-view.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'view',
    pathMatch: 'full'
  },
  {
    path: 'view',
    component: PaymentViewComponent
  },
  {
    path: 'update/:id',
    component: PaymentUpdateComponent
  },
  {
    path: 'cheque/view',
    component: ChequePaymentViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PaymentRoutingModule { }
