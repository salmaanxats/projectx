import { PartialPaymentModel } from './../model/partialPaymentModel';
import { PaymentModel } from './../model/paymentModel';
import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { PaymentService } from '../service/payment.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap';
import { ChequePaymentModel, ChequeModel } from '../model/checkPaymentModel';
import { ChequePaymentService } from '../service/chequePayment.service';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { ChequeStatus } from 'src/app/core/enums/chequeStatus';
import { isNull, isUndefined, isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-payment-update',
  templateUrl: './payment-update.component.html',
  styleUrls: ['./payment-update.component.scss']
})
export class PaymentUpdateComponent implements OnInit {

  id: number = 0;
  paymentModel = new PaymentModel();
  partialPaymentModel = new PartialPaymentModel();
  chequePaymentModel = new ChequePaymentModel();
  chequeModel = new ChequeModel();
  selectedChequeModel = new ChequeModel();
  chequeForm: FormGroup;
  paymentForm: FormGroup;
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  isUpdateCheque = false;
  isFormSubmitted = false;
  isChequePaymentFormSubmitted = false;
  isBlocked = false;

  sectionName = "Payment";


  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    protected toasterService: ToasterService, private paymentService: PaymentService,
    private modalService: BsModalService, private chequePaymentService: ChequePaymentService) {
  }

  ngOnInit() {
    this.createForm();
    this.createChequeForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (!isZero(this.id) && !isUndefined(this.id)) {
        this.getPaymentById();
      }
    });
  }

  openModal(template: TemplateRef<any>, isUpdate: boolean, chequeModel: ChequeModel) {
    if (!isUpdate) {
      this.modalRef = this.modalService.show(template, this.config);
    }
    else {
      if (chequeModel.chequeStatus.toString() != 'Realized') {
        this.modalRef = this.modalService.show(template, this.config);
      } else {
        this.toasterService.warning("Cannot edit realized cheques");
        return;
      }
    }
    this.isUpdateCheque = isUpdate;
    if (isUpdate) {
      this.selectedChequeModel = chequeModel;
      this.patchChequePayment(chequeModel);
    } else {
      this.createChequeForm();
    }
  }

  hideModal() {
    this.resetChequePaymentForm();
    this.modalRef.hide();
    this.isUpdateCheque = false;
  }

  createForm() {
    this.paymentForm = this.fb.group({
      paidAmount: ['', [Validators.required]],
      paymentMethod: [1, [Validators.required]],
    });
  }

  createChequeForm() {
    this.chequeForm = this.fb.group({
      chequeDate: [new Date, [Validators.required]],
      bank: [''],
      branch: [''],
      chequeNumber: ['', [Validators.required]],
      amount: ['', [Validators.required]],
      chequeStatus: ['']
    });
  }

  getPaymentById() {
    this.paymentService.getById(this.id)
      .subscribe((res: any) => {
        this.paymentModel = res;
        this.resetChequePaymentForm();
      }, error => {
        this.toasterService.errorLoading(error, this.sectionName);
      });
  }

  patchChequePayment(chequeModel: ChequeModel) {
    this.chequeForm.patchValue({
      chequeDate: new Date(chequeModel.chequeDate),
      bank: chequeModel.bank,
      branch: chequeModel.branch,
      chequeNumber: chequeModel.chequeNumber,
      amount: chequeModel.amount,
      chequeStatus: this.getChequeStatus(chequeModel.chequeStatus)
    });
  }

  getChequeStatus(chequeStatus) {
    if (chequeStatus == "Unrealized")
      return 1;
    if (chequeStatus == "Realized")
      return 2;
    if (chequeStatus == "Bounced")
      return 3;
    if (chequeStatus == "UnPresented")
      return 4;
  }

  addPayment() {
    this.isFormSubmitted = true;
    if (this.paymentForm.invalid) return;
    this.partialPaymentModel = Object.assign({}, this.partialPaymentModel, this.paymentForm.value);
    this.partialPaymentModel.paymentId = this.id;
    this.isBlocked = true;
    this.paymentService.save(this.partialPaymentModel)
      .subscribe((res: any) => {
        this.toasterService.successfullyCreated(this.sectionName);
        this.isBlocked = false;
        this.getPaymentById();
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorSaving(error, this.sectionName);
        });
  }

  resetChequePaymentForm() {
    this.createChequeForm();
  }

  addCheque() {
    this.isChequePaymentFormSubmitted = true;
    if (this.chequeForm.invalid) return;
    this.chequeModel = Object.assign({}, this.chequeModel, this.chequeForm.value);
    this.chequePaymentModel.paymentId = this.id;
    this.chequePaymentModel.paymentMethod = 2;
    this.chequeModel.chequeStatus = ChequeStatus.UnPresented;
    this.chequePaymentModel.paidAmount = this.chequeModel.amount;
    this.chequePaymentModel.chequePaymentModel = this.chequeModel;
    this.isBlocked = true;
    this.chequePaymentService.addCheque(this.chequePaymentModel)
      .subscribe((res: any) => {
        this.toasterService.successfullyCreated("Cheque")
        this.modalRef.hide();
        this.getPaymentById();
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorSaving(error, "Cheque");
        });
  }

  updateCheque() {
    this.isChequePaymentFormSubmitted = true;
    if (this.chequeForm.invalid) return;
    this.chequeModel = Object.assign({}, this.chequeModel, this.chequeForm.value);
    this.chequeModel.id = this.selectedChequeModel.id;
    this.chequeModel.partialPaymentId = this.selectedChequeModel.partialPaymentId;
    this.isBlocked = true;
    this.chequePaymentService.update(this.chequeModel)
      .subscribe((res: any) => {
        this.toasterService.successfullyUpdated("Cheque");
        this.modalRef.hide();
        this.isUpdateCheque = false;
        this.getPaymentById();
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorUpdating(error, "Cheque");
        });
  }
}
