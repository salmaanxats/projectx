import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { CategoryService } from '../service/category.service';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { isEmpty } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category-view.component.scss']
})
export class CategoryViewComponent implements OnInit {

  categories = [];
  searchTerm$ = new Subject<string>();
  searchRequestModel = new SearchRequestModel(8);

  isBlocked = false;

  constructor(private fb: FormBuilder, private router: Router,
    private categoryService: CategoryService, protected toasterService: ToasterService) {
    this.searchTerm$.pipe(debounceTime(300), distinctUntilChanged()).subscribe(data => {
      if (!isEmpty(data) && data)
        this.searchRequestModel.searchTerm = data;
      else {
        this.searchRequestModel.searchTerm = '';
      }
      this.getCategories();
    });
  }

  ngOnInit() {
    this.getCategories();
  }

  getCategories() {
    this.isBlocked = true;
    this.categoryService.get(this.searchRequestModel)
      .subscribe((res: any) => {
        this.categories = res.items;
        this.searchRequestModel.totalRecords = res.totalRecordCount;
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, "categories");
        });
  }

  editCategory(id: number) {
    this.router.navigate(['/category/update', id]);
  }

  public pageChanged(event: any): void {
    this.searchRequestModel.skip = (event.page - 1) * this.searchRequestModel.take;
    this.searchRequestModel.currentPage = event.page;
    this.getCategories();
  }
}
