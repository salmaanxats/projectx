import { CategoryModule } from './../category.module';
import { CategoryModel } from './../model/categoryModel';
import { Injectable } from '@angular/core';
import { BaseService } from 'src/app/core/services/base.service';
import { HttpClient } from '@angular/common/http';
import { setup, setupReadAll } from 'src/app/shared/utility/extension';
import { HttpOperation } from 'src/app/core/enums/httpOperation';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';
import { IBaseImplementService } from 'src/app/core/services/base-implement.service';

@Injectable({
  providedIn: 'root'
})

export class CategoryService extends BaseService implements IBaseImplementService<CategoryModule> {

  baseLink: string;

  constructor(protected http: HttpClient) {
    super(http);
    this.baseLink = "category";
  }

  get(searchRequestModel: SearchRequestModel): any {
    return this.generateRequest(setupReadAll(this.baseLink, searchRequestModel), HttpOperation.Get)
  }
  getById(id: any): any {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Get)
  }
  save(body: CategoryModule) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Post, body)
  }
  delete(id: any) {
    return this.generateRequest(setup(`${this.baseLink}/${id}`), HttpOperation.Delete)
  }
  update(body: CategoryModule) {
    return this.generateRequest(setup(this.baseLink), HttpOperation.Put, body)
  }

}
