import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CategoryService } from '../service/category.service';
import { CategoryModel } from '../model/categoryModel';
import { ToasterService } from 'src/app/core/services/toaster.service';
import { isUndefined, isZero } from 'src/app/shared/utility/extension';

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.scss']
})
export class CategoryUpdateComponent implements OnInit {

  categoryForm: FormGroup;
  category = new CategoryModel();

  id: number;
  isFormSubmitted = false;
  isEdit = false;
  isBlocked = false;
  sectionName = "Category";

  constructor(private fb: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private categoryService: CategoryService, private toasterService: ToasterService) {
  }

  ngOnInit() {
    this.createForm();
    this.activatedRoute.params.subscribe((params: Params) => {
      this.id = params['id'];
      if (!isZero(this.id) && !isUndefined(this.id)) {
        this.isEdit = true;
        this.getCategoryById();
      }
    });
  }

  createForm() {
    this.categoryForm = this.fb.group({
      name: ['', [Validators.required]],
      description: [''],
    });
  }

  getCategoryById() {
    this.isBlocked = true;
    this.categoryService.getById(this.id)
      .subscribe((res: any) => {
        this.category = res;
        this.patchCategory(this.category);
        this.isBlocked = false;
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorLoading(error, this.sectionName);
        });
  }

  patchCategory(category: CategoryModel) {
    this.categoryForm.patchValue({
      name: category.name,
      description: category.description
    });
  }

  save() {
    this.isFormSubmitted = true;
    if (this.categoryForm.invalid) return;
    this.category = Object.assign({}, this.category, this.categoryForm.value);
    this.isBlocked = true;
    this.categoryService.save(this.category)
      .subscribe((res: any) => {
        this.toasterService.successfullyCreated(this.sectionName);
        this.isBlocked = false;
        this.router.navigate(['category/view']);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorSaving(error, this.sectionName);
        });
  }

  delete() {
    this.isBlocked = true;
    this.categoryService.delete(this.id)
      .subscribe((res: any) => {
        this.router.navigate(['category/view']);
        this.isBlocked = false;
        this.toasterService.successfullyDeleted(this.sectionName);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorDeleting(error, this.sectionName);
        });
  }

  update() {
    this.isFormSubmitted = true;
    if (this.categoryForm.invalid) return;
    this.category = Object.assign({}, this.category, this.categoryForm.value);
    this.isBlocked = true;
    this.categoryService.update(this.category)
      .subscribe((res: any) => {
        this.toasterService.successfullyUpdated(this.sectionName);
        this.isBlocked = false;
        this.router.navigate(['category/view']);
      },
        error => {
          this.isBlocked = false;
          this.toasterService.errorUpdating(error, this.sectionName);
        });
  }
}
