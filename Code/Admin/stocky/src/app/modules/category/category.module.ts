import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryViewComponent } from './category-view/category-view.component';
import { CategoryUpdateComponent } from './category-update/category-update.component';
import { CategoryRoutingModule } from './category-routing.module';
import { CategoryService } from './service/category.service';
import { SharedModule } from 'src/app/shared/shared.module';


@NgModule({
  declarations: [CategoryViewComponent, CategoryUpdateComponent],
  imports: [
    CommonModule,
    CategoryRoutingModule,
    SharedModule
  ],
  providers: [CategoryService]
})
export class CategoryModule { }
