import { SearchRequestModel } from '../models/search-request.model';
import { Constant } from './constant';

export function setup(actionurl: string) {
  return Constant.api + actionurl;
}

export function setupReadAll(actionurl: string, searchRequestModel: SearchRequestModel) {
  return Constant.api + actionurl + `?skip=${searchRequestModel.skip}&take=${searchRequestModel.take}&searchTerm=${searchRequestModel.searchTerm}&orderby=${searchRequestModel.orderBy}`;
}

export function renderImage(path: string, images: Array<string>) {
  let renderedImages = new Array<string>();
  images.forEach(element => {
    renderedImages.push(Constant.imagePath + `${path}/${element}`);
  });
  return renderedImages;
}

export function isArrayEmpty(obj) {
  return obj.length == 0 ? true : false;
}

export function isZero(obj) {
  return obj == 0 ? true : false;
}

export function isNull(obj) {
  return obj == null ? true : false;
}

export function isUndefined(obj) {
  return obj == undefined ? true : false;
}

export function isEmpty(obj) {
  return obj == '' ? true : false;
}

