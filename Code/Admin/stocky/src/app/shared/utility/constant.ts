import { environment } from 'src/environments/environment';

export class Constant {
  static readonly maximumAllowedImages: number = 5;
  static readonly api :string = environment.apiEndPoint;
  static readonly imagePath :string = environment.imagePath;
}
