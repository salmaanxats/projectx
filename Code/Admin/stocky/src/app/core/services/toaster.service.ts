import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class ToasterService {

  constructor(private toaster: ToastrService) { }

  public success(message: string, title: string = 'Success') {
    this.toaster.success(message, title);
  }

  public error(message: string, title: string = 'Error') {
    this.toaster.error(message, title);
  }

  public warning(message: string, title: string = 'Warning') {
    this.toaster.warning(message, title);
  }

  public info(message: string, title: string = 'Info') {
    this.toaster.info(message, title);
  }

  public successfullyCreated(message: string) {
    this.success(`${message} created successfully!`);
  }

  public successfullyUpdated(message: string) {
    this.success(`${message} updated successfully!`);
  }

  public successfullyDeleted(message: string) {
    this.success(`${message} deleted successfully!`);
  }

  public errorSaving(message: string, title: string) {
    this.error(message, `Failed to save ${title}`);
  }

  public errorUpdating(message: string, title: string) {
    this.error(message, `Failed to update ${title}`);
  }

  public errorLoading(message: string, title: string) {
    this.error(message, `Failed to load ${title}`);
  }

  public errorDeleting(message: string, title: string) {
    this.error(message, `Failed to delete ${title}`);
  }
}
