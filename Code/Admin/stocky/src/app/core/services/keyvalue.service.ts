import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpOperation } from '../enums/httpOperation';
import { setup } from '../../shared/utility/extension';

@Injectable({
  providedIn: 'root'
})
export class KeyvalueService extends BaseService {

  constructor(httpClient: HttpClient) {
    super(httpClient);
  }

  getCustomerKeyvalue() {
    return this.generateRequest(setup("customer/keyvalue"), HttpOperation.Get);
  }

  getProductKeyValue() {
    return this.generateRequest(setup("product/keyvalue"), HttpOperation.Get);
  }

}
