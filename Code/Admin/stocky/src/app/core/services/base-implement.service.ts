import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SearchRequestModel } from 'src/app/shared/models/search-request.model';

export interface IBaseImplementService<T> {
  get(searchRequestModel: SearchRequestModel): any;
  getById(id): any;
  save(body: T);
  delete(id);
  update(body: T);
}
