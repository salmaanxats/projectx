export enum ChequeStatus{
  Unrealized = 1,
  Realized = 2,
  Bounced = 3,
  UnPresented = 4
}
