import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  isPaymentClicked: boolean = false;
  isAdministrationClicked: boolean = false;
  isInventoryClicked: boolean = false;
  constructor() { }

  ngOnInit() {
  }

  navClicked(navItem: string) {
    if (navItem == 'isPaymentClicked') {
      this.isPaymentClicked == true;
    } else if (navItem == 'isAdministrationClicked') {
      this.isAdministrationClicked == true;
    } else if (navItem == 'isInventoryClicked') {
      this.isInventoryClicked == true;
    }
  }
}
